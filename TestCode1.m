function answer = test_code1()
    function code = code1()
        disp('Adding 1 to 1.')
        code = 1+1
    end
disp('Testing code1.')
answer = (code1() == 2)
end
